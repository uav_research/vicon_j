#include "ros/ros.h"
#include "std_msgs/String.h"
#include "udp.h"
#include "vicon.h"
#include <vicon_j/vicon.h>		// package name / message name
#include "vicon_j/position.h"
#include "geometry_msgs/PoseStamped.h"
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Scalar.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <assert.h>

//Desired position
float dx=0; float dy=0; float dz=1; 
//Desired angle || DO NOT USE YET
float dtx=0; float dty=0; float dtz=0;
//Actual position
float x=5; float y=5; float z=5;
//Angles in Eular radian 
tf2Scalar pitch = 5; tf2Scalar roll = 5; tf2Scalar yaw = 5;

bool updatePosition(vicon_j::position::Request &req,
		    vicon_j::position::Response &res){

	if(dz<0)dz=0; if(dz>2)dz=2; if(dx<-3)dx=-3; if(dx>2)dx=2; if(dy<-2)dy=-2; if(dy>2)dy=2;
	dx = req.x;
	dy = req.y;
 	dz = req.z;
	dtx = req.thetax;
	dty = req.thetay;
	dtz = req.thetaz;
	if(x>=dx-0.2 && x<=dx+0.2)
		if(y>=dy-0.2 && y<=dy+0.2)
			res.inPosition = true;
		else
			res.inPosition = false;
return true;
}


int main(int argc, char **argv){

  ros::init(argc,argv,"vicon_listen");
  ros::NodeHandle n;
  ros::Publisher pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/mocap/pose",10);
  ros::Publisher pub_pos = n.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local",100);
  ros::ServiceServer service = n.advertiseService("quad_position",updatePosition);
  ros::Rate loop_rate(200);

  udp_struct udpNavLog;
  udp_struct udpState;
  FRAME_DATA VICON_frame;
  PVA_DATA   pva;
  udpServer_Init(&udpState,7901,0);
  init_frame(&VICON_frame, 27); //Set 27 to packet length if you change it
  int count = 1;
  geometry_msgs::PoseStamped msg;
  geometry_msgs::PoseStamped position;  
  tf2::Quaternion quat;

while(ros::ok()){
        get_vicon_packet(&VICON_frame, &udpState, &pva);
   
	msg.header.stamp=ros::Time::now();
	position.header.stamp = ros::Time::now();
	msg.header.seq=count;
	position.header.seq=count;
	msg.header.frame_id=1;
	position.header.frame_id=1;

	x = pva.position[0];
	y = pva.position[1]*-1;
	z = pva.position[2]*-1;
	pitch = pva.orientation[0];
	roll = pva.orientation[1];
	yaw = pva.orientation[2];
	quat.setEuler(yaw,pitch,roll);

	msg.pose.position.x = x;
	msg.pose.position.y = y;
	msg.pose.position.z = z;
	msg.pose.orientation.x = quat.x();
	msg.pose.orientation.y = quat.y();
	msg.pose.orientation.z = quat.z();
	msg.pose.orientation.w = quat.w();

	position.pose.position.x = dx;
	position.pose.position.y = dy;
	position.pose.position.z = dz;
	position.pose.orientation.z = 0;

    pub.publish(msg);
    pub_pos.publish(position);
    ros::spinOnce();
    count++;
    loop_rate.sleep();
  }

  return 0;
}
