#Vicon_j ROS package for receiving vicon data on board a drone.#

Requires an on board computer running ROS.

Package receives data from vicon then pushes it a pixhawk running
the px4 stack via mavros.

Video on how to use this: https://www.youtube.com/watch?v=hjmDPUNs1DM